package raumschiffSchmaumschiff;

public class Kapitaen {
	private String name;
	private int kapitaenSeit;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKapitaenSeit() {
		return kapitaenSeit;
	}

	public void setKapitaenSeit(int kapitaenSeit) {
		this.kapitaenSeit = kapitaenSeit;
	}

}
