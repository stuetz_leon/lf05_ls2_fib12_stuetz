package raumschiffSchmaumschiff;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Leon
 *
 * Fantastisches Raumschiff
 */
public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInprozent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private List<Ladung> ladungsverzeichnis; // wir benutzen in diesem Fall die Superklasse Liste anstelle von ArrayList
	private List<String> broadcastKommunikator;

	public Raumschiff() {
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schutzschildeInProzent,
			int huelleInprozent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String name) {

		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schutzschildeInProzent;
		this.huelleInprozent = huelleInprozent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = name;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
		this.broadcastKommunikator = new ArrayList<String>();
	}

	/**
	 * Fuegt die übergebene Ladung dem Ladungsverzeichnis hinzu
	 * Falls die Ladung bereits existiert, wird die Menge ueberschrieben
	 *
	 * @param hinzuzufügende Ladung
	 */
	public void addLadung(Ladung neueLadung) {

		for (final Ladung aktuelleLadung : ladungsverzeichnis) {
			if (aktuelleLadung.getBezeichnung() == neueLadung.getBezeichnung()) {
				aktuelleLadung.setMenge(aktuelleLadung.getMenge() + neueLadung.getMenge());
				return;
			}
		}
		this.ladungsverzeichnis.add(neueLadung);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + "\n energieversorgungInProzent="
				+ energieversorgungInProzent + "\n schildeInProzent=" + schildeInProzent + "\n huelleInprozent="
				+ huelleInprozent + "\n lebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent
				+ "\n androidenAnzahl=" + androidenAnzahl + "\n schiffsname=" + schiffsname + "\n");

		for (final Ladung ladung : ladungsverzeichnis) {
			builder.append(ladung.toString() + "\n");
		}

		for (final String string : broadcastKommunikator) {
			builder.append(string + "\n");
		}
		return builder.toString();
	}

	/**
	 * Der übergebene Parameter wird als Nachricht an alle gesendet
	 *
	 * @param Nachricht
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
	}

	/**
	 * Das Ladungsverzeichnis wird auf der Konsole ausgegeben
	 */
	public void ladungsverzeichnisAusgeben() {
		for (final Ladung ladung : ladungsverzeichnis) {
			System.out.println(ladung.toString());
		}
	}


	/**
	 * Photonentorpedos werden auf das übergeben Raumschiff abgeschossen
	 * Ist die Anzahl der verfuegbaren Photonentorpedos kleiner gleich 0, wird nur ein Klickgeraeusch ausgegeben
	 *
	 * @param zielRaumschiff
	 */
	public void photonentorpedosSchiessen(Raumschiff zielRaumschiff) {
		if (photonentorpedoAnzahl <= 0) {
			broadcastKommunikator.add("-=Click=-");
		} else {
			photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(zielRaumschiff);
		}
	}

	/**
	 * Reparatur am eigenen Raumschiff wird an allen angegebenen Strukturen durchgeführt
	 *
	 * @param schutzschilde Sollen schutzschile repariert werden
	 * @param energieversorung Soll energieversorung repariert werden
	 * @param schiffshuelle Soll schiffshuelle repariert werden
	 * @param anzahlDroiden Die einzusetzende anzahlDroiden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorung, boolean schiffshuelle,
			int anzahlDroiden) {
		final double zufallsZahl = Math.random() * 100;
		int anzahlZuReparierenderStrukturen = 0;
		if (schutzschilde) {
			anzahlZuReparierenderStrukturen++;
		}
		if (energieversorung) {
			anzahlZuReparierenderStrukturen++;
		}
		if (schiffshuelle) {
			anzahlZuReparierenderStrukturen++;
		}

		if (anzahlDroiden > getAndroidenAnzahl()) {
			anzahlDroiden = getAndroidenAnzahl();
		}

		final int prozentualeBerechnung = (int) ((zufallsZahl * anzahlDroiden) / anzahlZuReparierenderStrukturen);

		if (schutzschilde) {
			setSchildeInProzent(getSchildeInProzent() + prozentualeBerechnung);
		}
		if (energieversorung) {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + prozentualeBerechnung);
		}
		if (schiffshuelle) {
			setHuelleInprozent(getHuelleInprozent() + prozentualeBerechnung);
		}
	}

	/**
	 * Gibt eine Liste aller Logbucheintraege zurueck
	 *
	 * @return Liste aller Logbucheintraege
	 */
	public List<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}

	private void treffer(Raumschiff zielRaumschiff) {
		nachrichtAnAlle(zielRaumschiff.getSchiffsname() + " wurde getroffen");
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInprozent() {
		return huelleInprozent;
	}

	public void setHuelleInprozent(int huelleInprozent) {
		this.huelleInprozent = huelleInprozent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;

	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String name) {
		this.schiffsname = name;
	}

}